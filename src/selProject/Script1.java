package selProject;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.server.handler.ImplicitlyWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.bytebuddy.utility.RandomString;

public class Script1 {

	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		launchApp();
		login();
		Thread.sleep(2000);
		
		By userNameIconBtn = By.xpath("//button//span//span//mat-icon");
		clickOn(userNameIconBtn);

		Thread.sleep(2000);
		By usernameDisplayedTxt = By.xpath("//span[text()='Signed in as']/../span[2]");

		System.out.println(getText(usernameDisplayedTxt));

		driver.navigate().refresh();
		Thread.sleep(5000);
		By creatJobLnk = By.xpath("//span[text()=' Create Job ']");
		clickOn(creatJobLnk);
	
		fillDeviceDetails();
		fillProblemDetails();
		fillCustomerDetails();
		
		WebDriverWait w = new WebDriverWait(driver,3);
		By text = By.xpath("//span[contains(text(),'Job created successfully')]");
		WebDriverWait wait = new WebDriverWait(driver, 5);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(text));
		 System.out.println(getText(text));

	}

	public static void enterText(By locator, String data) {
		WebElement element = driver.findElement(locator);
		element.clear();
		element.sendKeys(data);
	}

	public static void clickOn(By locator) {
		WebElement element = driver.findElement(locator);
		element.click();
	}

	public static String getText(By locator) {
		WebElement element = driver.findElement(locator);
		String capturedText = element.getText();
		return capturedText;
	}

	public static String getIMEINumber() {
		String imeiNumber = RandomStringUtils.randomNumeric(14);
		return imeiNumber;
	}

	public static String getDate() {
		int day, month, year;
		GregorianCalendar date = new GregorianCalendar();
		day = date.get(Calendar.DAY_OF_MONTH);
		month = date.get(Calendar.MONTH)+1;
		year = date.get(Calendar.YEAR);
		String _date = "" + day + "/" + month + "/" + year;
		System.out.println(_date);
		return _date;
	}
	
	public static void launchApp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\patil\\Downloads\\Compressed\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://phoenix.testautomationacademy.in/");

		driver.manage().window().maximize();
	}
	
	public static void login() throws InterruptedException {
		By usernameTextBox = By.id("username");
		By passwordTextBox = By.id("password");
		By signInBtn = By.xpath("//span[text()=' Sign in ']");

		enterText(usernameTextBox, "iamfd");
		enterText(passwordTextBox, "password");

		Thread.sleep(5000);
		clickOn(signInBtn);

	}
	
	public static void fillDeviceDetails() throws InterruptedException {
		Thread.sleep(2000);
		By selectOEMOption = By.xpath("//span[text()='Select OEM']");
		clickOn(selectOEMOption);
		By selectGoogleOption = By.xpath("//span[text()=' Google ']");
		clickOn(selectGoogleOption);
		By selectProductNameOption = By.xpath("//span[text()='Select Product name']");
		clickOn(selectProductNameOption);
		By selectNexus2Option = By.xpath("//span[text()=' Nexus 2 ']");
		clickOn(selectNexus2Option);
		By selectModelName = By.xpath("//span[text()='Select Model name']");
		clickOn(selectModelName);
		Thread.sleep(2000);
   	    By selectNexus2BlueOption = By.xpath("//span[text()=' Nexus 2 blue ']");
		Thread.sleep(2000);
	    clickOn(selectNexus2BlueOption); Thread.sleep(5000);
		By imeiNumber = By.xpath("//input[@data-placeholder='0123456789']");
		enterText(imeiNumber, getIMEINumber());
		By dateOfPurchase = By.xpath("//input[@data-placeholder='dd/mm/yyyy']");
		clickOn(dateOfPurchase);
		enterText(dateOfPurchase, getDate());
		By warrantyStatusDrpDwn = By.xpath("//span[text()='Select Warranty Status']");
		clickOn(warrantyStatusDrpDwn);
		By inWarrantyOption = By.xpath("//span[text()=' In Warrenty ']");
		clickOn(inWarrantyOption);
		By selectPlatformDrpDwn = By.xpath("//span[text()='Select Platform']");
		clickOn(selectPlatformDrpDwn);
		  Thread.sleep(2000);
		By selectFstOption = By.xpath("//span[text()=' FST ']");
		clickOn(selectFstOption);
	}
	
	public static void fillProblemDetails() {
		By selectProblemDrpDwn = By.xpath("//span[text()='Select Problem']");
		clickOn(selectProblemDrpDwn);
		By selectOptionSmartPhoneIsRunningSLowOption = By.xpath("//span[text()=' Smartphone is running slow ']");
		clickOn(selectOptionSmartPhoneIsRunningSLowOption);
		By remarksTextBox = By.xpath("//input[@data-placeholder='Remarks']");
		enterText(remarksTextBox, "Test");
	}
	
	public static void fillCustomerDetails() throws InterruptedException {
		By serviceLocationPinCode = By.xpath("//input[@formcontrolname='pincode']");
		enterText(serviceLocationPinCode,"400075");
		By selectServiceLocationDrpDwn = By.xpath("//span[text()='Select Service Location']");
		clickOn(selectServiceLocationDrpDwn);
		By selectB2XOption = By.xpath("//span[text()=' B2X Solutions ']");
		clickOn(selectB2XOption);
		By firstNameTextBox = By.xpath("//input[@formcontrolname='firstName']");
		enterText(firstNameTextBox, "Jugal");
		By lastNameTextBox = By.xpath("//input[@formcontrolname='lastName']");
		enterText(lastNameTextBox, "Patil");
		By contactNoTextBox = By.xpath("//input[@formcontrolname='contactNo']");
		enterText(contactNoTextBox, "9049308935");
		By alternateContactNoTextBox = By.xpath("//input[@formcontrolname='alternateContactNo']");
		enterText(alternateContactNoTextBox, "9049308935");
		By emailIdTextBox = By.xpath("//input[@formcontrolname='emailId']");
		enterText(emailIdTextBox, "test@test.com");
		By alternateEmailIdTextBox = By.xpath("//input[@formcontrolname='alternateEmailId']");
		enterText(alternateEmailIdTextBox, "test@test.com");
		By flatNoTextBox = By.xpath("//input[@formcontrolname='flatNo']");
		enterText(flatNoTextBox, "A903");
		By apartmentName = By.xpath("//input[@formcontrolname='apartmentName']");
		enterText(apartmentName, "Test Apartment");
		By streetNameTextBox = By.xpath("//input[@formcontrolname='streetName']");
		enterText(streetNameTextBox, "M G Road");
		By landmarkTextBox = By.xpath("//input[@formcontrolname='landmark']");
		enterText(landmarkTextBox, "Infront of Subway");
		By areaTextBox = By.xpath("//input[@formcontrolname='area']");
		enterText(areaTextBox, "Test Area");
		By pinTextBox = By.xpath("//input[@formcontrolname='custPincode']");
		enterText(pinTextBox, "444444");
		By stateTextBox = By.xpath("//input[@formcontrolname='state']");
		  Thread.sleep(2000);
		enterText(stateTextBox, "Maharashtra");
		By submitBtn = By.xpath("//span[text()=' Submit ']");
		  Thread.sleep(2000);
		clickOn(submitBtn);
	}

}
